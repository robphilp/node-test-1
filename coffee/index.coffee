express = require('express')
app = express()
http = require('http').Server(app)
io = require('socket.io')(http)
path = require('path')

app.use(express.static('public'))

app.get('/', (req, res) ->
    res.sendFile(path.resolve(__dirname + '/../index.html'))
)

io.on('connection', (client) ->
    people = {}
    conn_count = 0;
    disconnect = ->
        console.log('user disconnected')
        client.broadcast.emit('message', "#### #{people[client.id].name} HAS LEFT THE CHAT ####")
        delete people[client.id]
        conn_count--
        if(conn_count > 0)
            client.broadcast.emit('people_update', people)
    
    join = (name) ->
        people[client.id] = { name: name }
        conn_count++
        io.emit('message', "#### #{name} HAS JOINED THE CHAT ####")
        io.emit('people_update', people)

    message = (msg) ->
        # console.log('message: ' + msg);
        io.emit('message', "#{people[client.id].name} #{msg}");

    console.log('a user connected')
    client.on('disconnect', disconnect)
    client.on('join', join)
    client.on('message', message)
)

http.listen(8004, ->
    console.log('listening on *:8004')
)