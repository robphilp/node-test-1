var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);

app.use(express.static('public'));

app.get('/', function(req, res){
	res.sendFile('index.html', {root: __dirname});
});

IOHandler = function() {};
IOHandler.people = {};
IOHandler.connect = function(client) {
    disconnect = function() {
        console.log('user disconnected');
        client.broadcast.emit('message', '####' + IOHandler.people[client.id].name + ' HAS LEFT THE CHAT ####');
        delete IOHandler.people[client.id];
        client.broadcast.emit('people_update', IOHandler.people);
    };
    join = function(name) {
        IOHandler.people[client.id] = { name: name }
        io.emit('message', '####' + name + ' HAS JOINED THE CHAT ####');
        io.emit('people_update', IOHandler.people); 
    }; 
    message = function(msg) {
        // console.log('message: ' + msg);
        io.emit('message', IOHandler.people[client.id].name + ': ' + msg);
    };
	console.log('a user connected');
	client.on('disconnect', disconnect);
    client.on('join', join)
	client.on('message', message);
};

io.on('connection', IOHandler.connect);

http.listen(8004, function(){
	console.log('listening on *:8004');
});